package wikrama.isyana

import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener {


    lateinit var lsAdapter : ListAdapter
    lateinit var  db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var intentIntegrator: IntentIntegrator

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnScanQR ->{
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btnGenerateQR ->{
                val barCodeEncoder = BarcodeEncoder()
                val bitmap = barCodeEncoder.encodeBitmap(edQrCode.text.toString(),
                    BarcodeFormat.QR_CODE,400,400)
                imageView2.setImageBitmap(bitmap)
            }
            R.id.btnSimpan ->{
                dialog.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("No",null)
                dialog.show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)
        if (intentResult!=null){
            edQrCode.setText(intentResult.contents)
        }else{
            Toast.makeText(this,"Dibatalkan",Toast.LENGTH_SHORT).show()
        }
        val strToken = StringTokenizer(edQrCode.text.toString(),",",false)
        edNIM.setText(strToken.nextToken())
        edNama.setText(strToken.nextToken())
        edProdi.setText(strToken.nextToken())
        super.onActivityResult(requestCode, resultCode, data)
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        db = DBOpenHelper(this).writableDatabase
        dialog = AlertDialog.Builder(this)
        intentIntegrator = IntentIntegrator(this)
        btnGenerateQR.setOnClickListener(this)
        btnScanQR.setOnClickListener(this)
        btnSimpan.setOnClickListener(this)
    }
    fun showDataBio(){
        var sql=""
        sql = "select nim as _id, nama, prodi from bioMhs"
        val c : Cursor = db.rawQuery(sql,null)
        adapter = SimpleCursorAdapter(this,R.layout.item_data_bio_mhs,c, arrayOf("_id", "nama", "prodi"), intArrayOf(R.id.txNIM, R.id.txNama, R.id.txProdi), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lvDatabase.adapter = adapter
    }
    fun getDBObject():SQLiteDatabase{
        return db
    }
    fun insertdatabio(nim : String, nama : String, prodi : String){
        var cv : ContentValues = ContentValues()
        cv.put("nim",nim)
        db.insert("bioMhs",null,cv)
        cv.put("nama",nama)
        db.update("bioMhs",cv,"nim=$nim",null)
        cv.put("prodi",prodi)
        db.update("bioMhs",cv,"nim=$nim",null)
        edQrCode.setText("")
        edNIM.setText("")
        edNama.setText("")
        edProdi.setText("")
        showDataBio()
    }
    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertdatabio(edNIM.text.toString(),edNama.text.toString(),edProdi.text.toString())
    }

    override fun onStart() {
        super.onStart()
        showDataBio()
    }

}
