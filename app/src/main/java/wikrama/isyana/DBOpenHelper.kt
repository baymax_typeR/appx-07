package wikrama.isyana

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context) : SQLiteOpenHelper (context,DB_Name,null,DB_Ver){
    override fun onCreate(db: SQLiteDatabase?) {
        val bioMhs = "create table bioMhs(nim text primary key, nama text , prodi text )"
        db?.execSQL(bioMhs)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object{
        val DB_Name = "aplikasi07"
        val DB_Ver = 1
    }

}